En este repositorio se aloja la presentación sobre el «Pricing del tipo markdown» que fue utilizada en la charla para los 
miembros y público en general del Grupo de Usuarios de R en Chile.

La sesión contó con las siguientes partes:

* Pricing: concepto general
* Función de elasticidad
* El problema de optimización
* Pricing tip omarkdown
* Referencias esenciales


